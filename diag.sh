#!/bin/bash
########################################################################################################################
# ver 1.1 By Phoenix Myers 1.03.2020                                                                                   #
#                                                                                                                      #
#                                                                                                                      #
# Example Usages:                                                                                                      #
#                                                                                                                      #
# diag 1           - shows basic diagnostic info for stated cluster                                                    #
# diag 1 tg        - shows basic diagnostic info for stated cluster AND latency details                                #
# diag 1 peers     - shows ONLY # SIP peers on px servers in the stated cluster                                        #
# diag allpeers    - shows ONLY # SIP peers on px servers ALL CLUSTERS, no other details displayed                     #
# diag allfast     - Shows the same thing with allpeers but doesnt print hostnames (Faster)                            #
# diag $1 $2 $3    - $3 argument will modify the SSH timeout option. ie. diag 4 tg 1 (1 second timeout)                #
#                                                                                                                      #
########################################################################################################################
echo "Bash version ${BASH_VERSION}"

if [ -z "$1" ]
then
	echo "So, Which cluster?"
	echo
	echo
	head -15 `which diag.sh` | grep -v '#!/bin/bash'
	exit 0;
fi

if [ "$1" = "help" ]; then
	echo
	head -15 `which diag.sh` | grep -v '#!/bin/bash'
	exit 0;
fi

server="10.101.15.51"
numpings=500

# SSH Timeout setting - can speed up or slow down the script.
if [ -z "$3" ]
then
	timeout="2"  # 3 second timeout
	echo -e "SSH Timeout set to ${timeout} second(s)."
else
	timeout="$3" # Specific seconds for timeout - only for SSH timeouts
	echo -e "SSH Timeout set to ${3} second(s)."
fi

if [ "$1" = "allpeers" ]; then
	for i in $(nmap -n -p 22 10.101.{1..13}.{5..6}|grep epor|awk '{print $NF}');
	do
	host=`ssh -o ConnectTimeout=${timeout} root@${i} cat /etc/hostname | cut -d "." -f1`
	pxpeers=`curl -s http://${i}:2288/Status/?textmode | head -4 | tail -1 | awk '{print $2}'`
	pxmaxpeers=`curl -s http://${i}:2288/Status/?textmode | head -4 | tail -1 | awk '{print $3}'`
	echo -e "${i} ${host} Peers: ${pxpeers}/${pxmaxpeers}"
	done
	echo
	exit 0;
else
	if [ "$1" = "allfast" ]; then
		echo -e "Who needs host names!"
		for i in $(nmap -n -p 22 10.101.{1..13}.{5..6}|grep epor|awk '{print $NF}');
		do
		pxpeers=`curl -s http://${i}:2288/Status/?textmode | head -4 | tail -1 | awk '{print $2}'`
		pxmaxpeers=`curl -s http://${i}:2288/Status/?textmode | head -4 | tail -1 | awk '{print $3}'`
		echo -e "${i} ${host} Peers: ${pxpeers}/${pxmaxpeers}"
		done
		echo
		exit 0;
	fi
fi

if [ "$2" = "peers" ]; then
for i in $(nmap -n -p 22 10.101.${1}.{5..6}|grep epor|awk '{print $NF}');
do
	host=`ssh -o ConnectTimeout=${timeout} root@${i} cat /etc/hostname | cut -d "." -f1`
	px1peers=`curl -s http://10.101.${1}.5:2288/Status/?textmode | head -4 | tail -1 | awk '{print $2}'`
	px1maxpeers=`curl -s http://10.101.${1}.5:2288/Status/?textmode | head -4 | tail -1 | awk '{print $3}'`
	px2peers=`curl -s http://10.101.${1}.6:2288/Status/?textmode | head -4 | tail -1 | awk '{print $2}'`
	px2maxpeers=`curl -s http://10.101.${1}.5:2288/Status/?textmode | head -4 | tail -1 | awk '{print $3}'`
	echo -e "PX1 Peers: ${px1peers}/${px1maxpeers} PX2 Peers: ${px2peers}/${px2maxpeers}"
	echo
	exit 0;
done
fi

echo -e "Results:"
for i in $(nmap -n -p 22 10.101.${1}.{2..9}|grep epor|awk '{print $NF}');
do
	if [ "`ping -c 1 -w 1 ${i}`" ]
	then
	  upt=`ssh -o ConnectTimeout=${timeout} root@${i} uptime | sed s/^.*up// | awk -F, '{ if ( $3 ~ /user/ ) { print $1 $2 } else { print $1 }}' | sed -e 's/:/\ hrs\ /' -e 's/ min//' -e 's/$/\ mins/' | sed 's/^ *//'`
	  ast=`ssh -o ConnectTimeout=${timeout} root@${i} ps -ef | grep '/usr/sbin/asterisk' | grep -vE 'grep|GREP|screen|SCREEN' | awk '{printf $2}'`
		mem=`ssh -o ConnectTimeout=${timeout} root@${i} free -h | head -2 | tail -1 | awk '{print "Mem Ava: " $4}'`
	 load=`ssh -o ConnectTimeout=${timeout} root@${i} uptime | awk '{print $10}' | cut -d "," -f 1`
	 swap=`ssh -o ConnectTimeout=${timeout} root@${i} free -h | head -4 | tail -1 | awk '{print "Swap: " \$3 }'`
	procs=`ssh -o ConnectTimeout=${timeout} root@${i} cat /proc/cpuinfo  | grep 'processor' --count`
	 host=`ssh -o ConnectTimeout=${timeout} root@${i} cat /etc/hostname | cut -d "." -f1`
	if test -z "$ast"
	then
		ast="\e[31mNONE\e[39m"
	fi
		echo -e "${i} \033[0;36m${host}\e[39m: ${upt} Load: ${load} ${mem} ${swap} Procs: ${procs} Astrisk PID: \033[0;32m${ast}\e[39m"
		#printf " Mem%: ${mem}/${ttlmem} %"
	else
		echo "Not Available"
	fi
done

px1peers=`curl -s http://10.101.${1}.5:2288/Status/?textmode | head -4 | tail -1 | awk '{print $2}'`
px1maxpeers=`curl -s http://10.101.${1}.5:2288/Status/?textmode | head -4 | tail -1 | awk '{print $3}'`
px2peers=`curl -s http://10.101.${1}.6:2288/Status/?textmode | head -4 | tail -1 | awk '{print $2}'`
px2maxpeers=`curl -s http://10.101.${1}.5:2288/Status/?textmode | head -4 | tail -1 | awk '{print $3}'`
#echo -e "PX1 Peers: ${px1peers}/${px1maxpeers} PX2 Peers: ${px2peers}/${px2maxpeers}"
echo -e "PX1 Peers: ${px1peers}/${px1maxpeers} "
#echo "$((px1peers))/$((px1maxpeers))*100 %"
echo -e "PX2 Peers: ${px2peers}/${px2maxpeers} "
#echo '$((px2peers))/$((px2maxpeers))*100 %'
echo

if [ -z "$2" ]
then
	echo -e "Latency: \e[31mNOT TESTED\e[39m"
	exit 0;
else
echo
	case "$2" in
		# Ideas: nmap into remote subnet and get some random internal IP's to test (workstations)
		tg)			loc="TG (HN)"
						lan="172.30.32.1"
						wan="181.115.38.18"
						net="172.30.32"
						;;
		sps)		loc="SPS (HN)"
						lan="172.30.65.1"
						wan="190.107.138.228"
						net="172.30.65"
						;;
		iccs)		loc="ICCS (Philipines)"
						lan="172.16.24.1"
						wan="115.42.124.50"
						net="172.16.24"
    				;;
		bpo)		loc="BPO (Philipines)"
						lan="172.20.12.1"
						wan="115.42.124.26"
						net="172.20.12"
	   				;;
		*) 			echo "Invalid Site. Exiting..."
						exit 0
						;;
	esac
	echo -e "Performing packet loss test [colo --> ${loc}]:"
	echo -ne "Testing LAN... "
	lpl=$(ssh -o ConnectTimeout=${timeout} root@${i} ping -f -c ${numpings} ${lan} | head -4 | tail -1 | awk '{print $6}');
	echo -e "${lpl} Loss"
	echo -ne "Testing WAN... "
	wpl=$(ssh -o ConnectTimeout=${timeout} root@${i} ping -f -c ${numpings} ${wan} | head -4 | tail -1 | awk '{print $6}');
	echo -e "${wpl} Loss"
	echo
: <<'TEMP'
	echo "TEST1 ${server}"
	ssh root@${server} 'bash -s' net=$net; << "EOF"
		echo -e "Connected to Colo, looking for IPs to test"
		echo -e "TEST 2"
		for x in $(nmap -n -p 3389 $net.{100..150}|grep epor|awk '{print $NF}');
		do
			echo \$x
		done
EOF
fi
TEMP
fi
: <<'HOLDING'
for x in $(nmap -n -p 80 \$net.{100..125}|grep epor|awk '{print $NF}');
do
	echo \$x
done

ssh root@${i} 'bash -s' << EOF
	A=\$(cat /proc/loadavg | cut -c1-15);
	B=\$(free -h | head -4 | tail -1 | awk '{print "Swap: " \$3 }');
	H=\$(echo \${HOSTNAME}| cut -d"." -f1);
	if [ "$oct" -ge 4 -a "$oct" -le 10 ]; then
		PLP=\$(ping -f -c 250 ${ip} | head -4 | tail -1 | awk '{print \$6}');
	elif [ "$TESTALL" == "Y" ]; then
		# THIS IS A SET TEST TO PROTECT THE NETWORK!
		PLP=\$(ping -f -c 250 ${ip} | head -4 | tail -1 | awk '{print \$6}');
	else
		PLP="N/A"
	fi
	if [ -z "$site" ]
	then
		echo "Host: \${H} Loadavg: \${A} \${B}";
	else
		echo "Host: \${H} Loadavg: \${A} \${B} Site: ${site} Loss: \${PLP}";
fi
EOF



declare -a IP_ARRAY
NMAP_OUTPUT=`nmap -n -p 3389 $net.{100..125}|grep epor|awk '{print $NF}'`
read -a IP_ARRAY <<< $NMAP_OUTPUT
IP_ARRAY=(${NMAP_OUTPUT})
printf '%s\n' "${IP_ARRAY[@]}"


HOLDING
